// �ukasz Fa�da
// Grupa 2, Informatyka Rok II
// Numer indeksu: 115973

#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <iomanip>
using namespace std;

clock_t poczatek = 0.0, koniec = 0.0;
double czas = 0.0;

class macierz
{
    private:
        double **MACIERZ;
        int WIERSZE, KOLUMNY;

    public:
        macierz(int wiersze_ = 0, int kolumny_ = 0)
        {
            MACIERZ = 0;
            stworz(wiersze_,kolumny_);
        }
        ~macierz()
        {
            for(int i = 0; i < WIERSZE; ++i)
            {
                delete [] MACIERZ[i];
            }
            delete[] MACIERZ;

            MACIERZ = 0;
            WIERSZE = 0;
            KOLUMNY = 0;
        }

        int	&wiersz(){return WIERSZE;}
        int	&kolumna(){return KOLUMNY;}
        const int &wiersz()const{return WIERSZE;}
        const int &kolumna()const{return KOLUMNY;}

        void stworz(int wiersze_, int kolumny_)
        {
            MACIERZ = new double*[wiersze_];

            WIERSZE = wiersze_;

            if(kolumny_ > 0)
            {
                KOLUMNY = kolumny_;
            }
            else
            {
                KOLUMNY = wiersze_;
            }

            for(int i = 0; i < WIERSZE; ++i)
            {
                MACIERZ[i] = new double[KOLUMNY];
            }

            for(int i = 0; i < WIERSZE; ++i)
            {
                for(int j = 0; j < KOLUMNY; ++j)
                {
                    MACIERZ[i][j] = 0;
                }
            }
        }
        
        macierz &operator = (const macierz &m)
        {
            if(this != &m)
            {
                stworz(m.WIERSZE, m.KOLUMNY);

                for(int i = 0; i < WIERSZE; ++i)
                {
                    for(int j = 0; j < KOLUMNY; ++j)
                    {
                        MACIERZ[i][j] = m.MACIERZ[i][j];
                    }
                }
            }
            return *this;
        }

        friend istream &operator >> (istream &we, macierz &m)
        {
            int wiersz, kolumna;

            we >> wiersz >> kolumna;
            m.stworz(wiersz, kolumna);

            for(int i = 0; i < m.WIERSZE; i++)
            {
                for(int j = 0; j < m.KOLUMNY; ++j)
                {
                    we >> m.MACIERZ[i][j];
                }
            }
            return we;
        }

        friend ostream &operator << (ostream &wy, const macierz &m)
        {
            wy << endl;
            for(int i = 0; i < m.WIERSZE; ++i)
            {
                for(int j = 0; j < m.KOLUMNY; ++j)
                {
                    wy << "   " << m.MACIERZ[i][j];
                }
                wy << endl;
            }
            return wy;
        }

        double *operator [] (int i)
        {
            return MACIERZ[i];
        }
        
//######    ***    WYPELNIANIE MACIERZY LOSOWOYMI WARTOSCIAMI    ***   #######################
	void WypelnijLosowo()
	{
		const int n = WIERSZE, m = KOLUMNY;

		for(int i = 0; i < n; ++i)
		{
			for(int j = 0; j < m; ++j)
			{
				MACIERZ[i][j] = (rand() % 1000 ) + 1;
			}
		}
	}

//######    ***    ZAPIS MACIERZY DO PLIKU    ***   #######################
	void zapis(ofstream &wyj)
	{
		const int n = WIERSZE, m = KOLUMNY;

		wyj << n << " " << m << endl;

		for(int i = 0; i < n; ++i)
		{
			for(int j = 0; j < m; ++j)
			{
				wyj << MACIERZ[i][j] << " ";
			}
			wyj << endl;
		}
	}
	
//######    ***    DODAWANIE    ***   ###################################################
        friend macierz operator + (const macierz &a, const macierz &b)
        {
            macierz m(a.KOLUMNY, a.WIERSZE);

            for(int i = 0; i < m.WIERSZE; ++i)
            {
                for(int j = 0; j < m.KOLUMNY; ++j)
                {
                    m.MACIERZ[i][j] = a.MACIERZ[i][j] + b.MACIERZ[i][j];
                }
            }
            return m;
        }

//######    ***    MNOZENIE    ***   ###################################################
        friend macierz operator * (const macierz &a, const macierz &b)
        {
            macierz m(a.WIERSZE, b.KOLUMNY);

            for(int i = 0; i < m.WIERSZE; ++i)
            {
                for(int j = 0; j < m.KOLUMNY; ++j)
                {
                    for(int k = 0; k < a.KOLUMNY; ++k)
                    {
                        m.MACIERZ[i][j] += a.MACIERZ[i][k] * b.MACIERZ[k][j];
                    }
                }
            }
            return m;
        }

//######    ***    MACIERZ TROJDIAGONALNA    ***   ######################################
    void trojdiagonalna(string di = "")
    {
        const int n = WIERSZE, m = KOLUMNY;
        macierz wektor(1,m);

		if(di == "")
		{
			wektor.WypelnijLosowo();
		}
		else
		{
			ifstream dii(di.c_str());
			dii >> wektor;
		}

		poczatek = clock();		//poczatek mierzenia czasu

        double a[n], b[n], c[n], d[n], x[n], BETA[n], GAMMA[n];
		a[0] = c[n-1] = 0;

        for(int i = 0; i < n; ++i)
        {
          b[i] = MACIERZ[i][i];
          d[i] = wektor[0][i];
        }
		for(int i = 1; i < n; ++i)
		{
			a[i] = MACIERZ[i][i-1];
		}
		for(int i = 0; i < n-1; ++i)
		{
			c[i] = MACIERZ[i][i+1];
		}

        BETA[0] = -(c[0] / b[0]);
        GAMMA[0] = (d[0] / b[0]);

        for(int i = 1; i < n; ++i)
        {
            BETA[i] = -(c[i] / (a[i] * BETA[i-1] + b[i]));
            GAMMA[i] = ((d[i] - (a[i] * GAMMA[i-1])) / ((a[i] * BETA[i-1]) + b[i]) );
        }

        x[n-1] = GAMMA[n-1];

        for(int i = n-2; i >= 0; --i)
        {
            x[i] = ((BETA[i] * x[i+1]) + GAMMA[i]);
        }

        for(int i = 0; i < n; ++i)
        {
            cout << "| " << i+1 << " - " << x[i] << "|" << endl;
        }

        
        koniec = clock();		// koniec mierzenia czasu
		czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
		cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
    }

//######    ***    MACIERZ ODWROTNA    ***   ###################################################
    void odwroc()
    {
		poczatek = clock();		//poczatek mierzenia czasu
		
        const int n = WIERSZE;
        double c, d;
        double **TYM = new double*[n];

        for(int i = 0; i < n; ++i)
        {
            TYM[i] = new double[2*n];
        }
        for(int i = 0; i < n; ++i)
        {
            for(int j = 0; j < n; ++j)
            {
                TYM[i][j] = MACIERZ[i][j];
            }
        }
        for(int i = 0; i < n; ++i)
        {
            for(int j = n; j < 2*n; ++j)
            {
                TYM[i][j] = 0;
            }
        }
        for(int i = 0; i < n; ++i)
        {
            TYM[i][n+i] = 1;
        }
        for(int s = 0; s < n; ++s)
        {
            c = TYM[s][s];

            TYM[s][s] = TYM[s][s] - 1;

            for(int j = s+1; j < 2*n; ++j)
            {
                d = TYM[s][j] / c;

                for(int i = 0; i < n; ++i)
                {
                    TYM[i][j] = TYM[i][j] - d * TYM[i][s];
                }
            }
        }

        for(int i = 0; i < n; ++i)
        {
            for(int j = n; j < 2*n; ++j)
            {
                cout << TYM[i][j] << " ";
            }
            cout << endl;
        }

        
        koniec = clock();		// koniec mierzenia czasu
		czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
		cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
		
		for(int i = 0; i < n; ++i)
		{
			delete [] TYM[i];
		}
		delete [] TYM;
    }

//######    ***    WYZNACZNIK MACIERZY    ***   ###################################################
    void wyznacznik()
    {
		poczatek = clock();		//poczatek mierzenia czasu

        const int n = WIERSZE;
        double wyz = 0.0;
        double **A = new double*[n];
        double **B = new double*[n];

        for(int i = 0; i < n; ++i)
        {
            A[i] = new double[n];
            B[i] = new double[n];
        }
        for(int i = 0; i < n; ++i)
        {
            for(int j = 0; j < n; ++j)
            {
                A[i][j] = MACIERZ[i][j];
            }
        }
        wyz = A[0][0];
        for(int s = 0; s < n-1; ++s)
        {
            for(int j = s+1; j < n; ++j)
            {
                B[s][j] = A[s][j] / A[s][s];
            }
            for(int i = s+1; i < n; ++i)
            {
                for(int j = s+1; j < n; ++j)
                {
                    A[i][j] = A[i][j] - (A[i][s] * B[s][j]);
                }
            }
            wyz = wyz * A[s+1][s+1];
        }
        cout << wyz;
        
        koniec = clock();		// koniec mierzenia czasu
		czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
		cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
        
		for(int i = 0; i < n; ++i)
		{
			delete [] A[i];
			delete [] B[i];
		}
		delete [] A;
		delete [] B;
    }

//######    ***    ELIMINACJA GAUSSA    ***   ###################################################
    void eliminacjagaussa()
    {
		poczatek = clock();		//poczatek mierzenia czasu

        const int n = WIERSZE;
        double **TYM = new double*[n];
        double X[n], sum;

        for(int i = 0; i < n; ++i)
        {
            TYM[i] = new double[n+1];
        }
        for(int i = 0; i < n; ++i)
        {
            for(int j = 0; j < n+1; ++j)
            {
                TYM[i][j] = MACIERZ[i][j];
            }
        }

        for(int s = 0; s < n-1; ++s)
        {
            for(int i = s+1; i < n; ++i)
            {
                for(int j = s+1; j < n+1; ++j)
                {
                    TYM[i][j] = TYM[i][j] - TYM[i][s] / TYM[s][s] * TYM[s][j];
                }
            }
        }

        X[n-1] = ( TYM[n-1][n] / TYM[n-1][n-1]);

        for(int i = n-2; i >= 0; --i)
        {
            sum = 0.0;

            for(int s = i+1; s < n; ++s)
            {
                sum = sum + (TYM[i][s] * X[s]);
            }
            X[i] = ((TYM[i][n] - sum) / TYM[i][i]);
        }

        for(int i = 0; i < n; ++i)
        {
            cout << "X" << i+1 << " = " << X[i] << endl ;
        }

        koniec = clock();		// koniec mierzenia czasu
		czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
		cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
		
		for(int i = 0; i < n+1; ++i)
		{
			delete [] TYM[i];
		}
		delete [] TYM;
    }
    
//######    ***    METODA GAUSSA-SEIDLA    ***   #######################
	void gaussseidl(string wek1 = "", string wek2 = "")
	{
		const int n = WIERSZE, m = KOLUMNY;
		const int iter = 10;
		double sum1, sum2;
		double **TYM = new double *[n], z[n], x[n];
		
		macierz wektor1(1,m), wektor2(1,m);
		wektor1.WypelnijLosowo();
		wektor2.WypelnijLosowo();

		if((wek1 == "") && (wek2 == ""))
		{
			wektor1.WypelnijLosowo();
			wektor2.WypelnijLosowo();
		}
		else
		{
			ifstream wekA(wek1.c_str());
			ifstream wekB(wek2.c_str());
			wekA >> wektor1;
			wekB >> wektor2;
		}
		
		poczatek = clock();		//poczatek mierzenia czasu
		
		for(int i = 0; i < n; ++i) 
		{
			TYM[i] = new double[n];
		}
		
		for(int i = 0; i < n; ++i) 
		{
			for(int j = 0; j < n; ++j) 
			{
				TYM[i][j] = MACIERZ[i][j];
			}
		}
		
		for(int i = 0; i < n; ++i) 
		{
			z[i] = wektor1[0][i];
			x[i] = wektor2[0][i];
		}
		
		for(int k = 0; k < iter; ++k) 
		{
			sum1 = 0;
			for(int j = 1; j < n; ++j) 
			{
				sum1 = sum1 + TYM[0][j] * x[j];
			}
			x[0] = sum1 + z[0];
			for(int i = 1; i < (n-1); ++i) 
			{
				sum1 = 0;
				for(int j = 0; j < i; ++j) 
				{
					sum1 = sum1 + TYM[i][j] * x[j];
				}
				sum2 = 0;
				for(int j = i; j < n; ++j)
				{
					sum2 = sum2 + TYM[i][j] * x[j];
				}
				x[i] = sum1 + sum2 + z[i];
			}
			sum2 = 0;
			for(int j = 0; j < (n-1); ++j) 
			{
				sum2 = sum2 + TYM[n-1][j] * x[j];
			}
			x[n-1] = sum2 + z[n-1];
		}

		for(int i = 0; i < n; ++i) 
		{
			cout << x[i] << endl;
		}

		 
        koniec = clock();		// koniec mierzenia czasu
		czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
		cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
				
		for(int i = 0; i < n; ++i)
		{
			delete [] TYM[i];
		}
		delete [] TYM; 
	}
};


int main()
{
	srand(time(NULL));

	string taknie, PL;
	int wybor = 0, roz = 0;


	do
	{
		cout << "Prosze wybrac jedno z ponizszych: \n";
		cout << "1 - macierz odwrotna\t\t"
			 << "4 - macierz trojdiagonalna\n"
			 << "2 - wyznacznik macierzy\t\t"
			 << "5 - dodawanie macierzy\n"
			 << "3 - eliminacja Gaussa\t\t"
			 << "6 - mnozenie macierzy\n"
			 << "\t\t\t\t7 - metoda Gaussa-Seidla\n";
		cout << "Odp. : ";
		cin >> wybor;


		if(wybor == 1)
		{
			cout << "\nMACIERZ ODWROTNA" << endl;
			cout << "Wczytac macierz z pliku, czy wypelnic wartosciami losowymi?" << endl;
			cout << "[P - wczytaj macierz z pliku, L - wypelnij losowo]" << endl;
			cout << "Odp. : ";
			cin >> PL;

			if(PL == "l" || PL == "L")
			{
				cout << "\nPodaj rozmiar n macierzy [nxn]: ";
				cin >> roz;
				cout << endl;
				macierz mOdwrotna(roz);
				mOdwrotna.WypelnijLosowo();
				//cout << "\nMacierz wypelniona losowymi liczbami: \n" << mOdwrotna << endl;
				cout << "\nMacierz wynikowa: \n\n";
				mOdwrotna.odwroc();

			}
			else if(PL == "p" || PL == "P")
			{
				cout << "\nnazwa pliku: macierzodwrotna.txt\n\n";
				ifstream Wczytaj("macierzodwrotna.txt");
				macierz mOdwrotna;
				Wczytaj >> mOdwrotna;
				//cout << "\nMacierz wypelniona liczbami z pliku: \n" << mOdwrotna << endl;
				cout << "\nMacierz wynikowa: \n";
				mOdwrotna.odwroc();
			}
		}

		else if(wybor == 2)
		{
			cout << "\nWYZNACZNIK MACIERZY" << endl;
			cout << "Wczytac macierz z pliku, czy wypelnic wartosciami losowymi?" << endl;
			cout << "[P - wczytaj macierz z pliku, L - wypelnij losowo]" << endl;
			cout << "Odp. : ";
			cin >> PL;


			if(PL == "l" || PL == "L")
			{
				cout << "\nPodaj rozmiar n macierzy [nxn]: ";
				cin >> roz;
				cout << endl;
				macierz mWyznacznik(roz);
				mWyznacznik.WypelnijLosowo();
				//cout << "\nMacierz wypelniona losowymi liczbami: \n" << mWyznacznik << endl;
				cout << "\nWynik: \n";
				mWyznacznik.wyznacznik();
			}
			else if(PL == "p" || PL == "P")
			{
				cout << "\nnazwa pliku: wyznacznikmacierzy.txt\n\n";
				ifstream Wczytaj("wyznacznikmacierzy.txt");
				macierz mWyznacznik;
				Wczytaj >> mWyznacznik;
				//cout << "\nMacierz wypelniona liczbami z pliku: \n" << mWyznacznik << endl;
				cout << "\nWynik: \n";
				mWyznacznik.wyznacznik();
			}
		}

		else if(wybor == 3)
		{
			cout << "\nELIMINACJA GAUSSA" << endl;
			cout << "Wczytac macierz z pliku, czy wypelnic wartosciami losowymi?" << endl;
			cout << "[P - wczytaj macierz z pliku, L - wypelnij losowo]" << endl;
			cout << "Odp. : ";
			cin >> PL;


			if(PL == "l" || PL == "L")
			{
				cout << "\nPodaj rozmiar n macierzy [nxn+1]: ";
				cin >> roz;
				cout << endl;
				macierz mGauss(roz,roz+1);
				mGauss.WypelnijLosowo();
				//cout << "\nMacierz wypelniona losowymi liczbami: \n" << mGauss << endl;
				cout << "\nMacierz wynikowa: \n";
				mGauss.eliminacjagaussa();
			}
			else if(PL == "p" || PL == "P")
			{
				cout << "\nnazwa pliku: eliminacjagaussa.txt\n\n";
				ifstream Wczytaj("eliminacjagaussa.txt");
				macierz mGauss;
				Wczytaj >> mGauss;
				//cout << "\nMacierz wypelniona liczbami z pliku: \n" << mGauss << endl;
				cout << "\nWynik: \n";
				mGauss.eliminacjagaussa();
			}
		}

		else if(wybor == 4)
		{
			cout << "\nMACIERZ TROJDIAGONALNA" << endl;
			cout << "Wczytac macierz z pliku, czy wypelnic wartosciami losowymi?" << endl;
			cout << "[P - wczytaj macierz z pliku, L - wypelnij losowo]" << endl;
			cout << "Odp. : ";
			cin >> PL;

			if(PL == "l" || PL == "L")
			{
				cout << "\nPodaj rozmiar n macierzy [nxn]: ";
				cin >> roz;
				cout << endl;
				macierz mTrojdiagonalna(roz);
				
				mTrojdiagonalna.WypelnijLosowo();
				//cout << "\nMacierz wypelniona losowymi liczbami: \n" << mTrojdiagonalna << endl;
				cout << "\nMacierz wynikowa: \n";
				mTrojdiagonalna.trojdiagonalna();
			}
			else if(PL == "p" || PL == "P")
			{
				cout << "\nnazwa pliku z macierza: trojdiagonalna.txt, z wektorem: wektor.txt\n\n";
				ifstream Wczytaj("trojdiagonalna.txt");
				macierz mTrojdiagonalna;
				Wczytaj >> mTrojdiagonalna;
				//cout << "\nMacierz wypelniona liczbami z pliku: \n" << mTrojdiagonalna << endl;
				cout << "\nMacierz wynikowa: \n";
				mTrojdiagonalna.trojdiagonalna("test.txt");
			}
		}

		else if(wybor == 5)
		{
			cout << "\nDODAWANIE MACIERZY" << endl;
			cout << "Wczytac macierze z pliku, czy wypelnic wartosciami losowymi?" << endl;
			cout << "[P - wczytaj macierze z pliku, L - wypelnij losowo]" << endl;
			cout << "Odp. : ";
			cin >> PL;

			if(PL == "l" || PL == "L")
			{
				cout << "\nPodaj rozmiar n macierzy [nxn]: ";
				cin >> roz;
				cout << endl;
				macierz mDodaj1(roz), mDodaj2(roz);
				mDodaj1.WypelnijLosowo();
				mDodaj2.WypelnijLosowo();
				//cout << "\nMacierz pierwsza wypelniona losowymi liczbami: \n" << mDodaj1 << endl
				//	 << "\nMacierz druga wypelniona losowymi liczbami: \n" << mDodaj2 << endl;
				cout << "\nMacierz wynikowa: \n";
				
				poczatek = clock();		//poczatek mierzenia czasu
				
				cout << mDodaj1 + mDodaj2;
				
				koniec = clock();		// koniec mierzenia czasu
				czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
				cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
				
			}
			else if(PL == "p" || PL == "P")
			{
				cout << "\nnazwa pliku 1: dodawanie1.txt\nnazwa pliku 2: dodawanie2.txt\n\n";
				ifstream Wczytaj1("dodawanie1.txt"), Wczytaj2("dodawanie2.txt");
				macierz mDodaj1, mDodaj2;
				Wczytaj1 >> mDodaj1;
				Wczytaj2 >> mDodaj2;
				//cout << "\nMacierz pierwsza wypelniona liczbami z pliku: \n" << mDodaj1 << endl
				//	 << "\nMacierz druga wypelniona liczbami z pliku: \n" << mDodaj2 << endl;
				cout << "\nMacierz wynikowa: \n";
				
				poczatek = clock();		//poczatek mierzenia czasu
		
				cout << mDodaj1 + mDodaj1;
				
				koniec = clock();		// koniec mierzenia czasu
				czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
				cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
			}
		}

		else if(wybor == 6)
		{
			cout << "\nMNOZENIE MACIERZY" << endl;
			cout << "Wczytac macierze z pliku, czy wypelnic wartosciami losowymi?" << endl;
			cout << "[P - wczytaj macierze z pliku, L - wypelnij losowo]" << endl;
			cout << "Odp. : ";
			cin >> PL;

			if(PL == "l" || PL == "L")
			{
				cout << "\nPodaj rozmiar n macierzy [nxn]: ";
				cin >> roz;
				cout << endl;
				macierz mMnoz1(roz), mMnoz2(roz);
				mMnoz1.WypelnijLosowo();
				mMnoz2.WypelnijLosowo();
				//cout << "\nMacierz pierwsza wypelniona losowymi liczbami: \n" << mMnoz1 << endl
				//	 << "\nMacierz druga wypelniona losowymi liczbami: \n" << mMnoz2 << endl;
				cout << "\nMacierz wynikowa: \n";
				
				poczatek = clock();		//poczatek mierzenia czasu
				
				cout << mMnoz1 + mMnoz2;
				
				koniec = clock();		// koniec mierzenia czasu
				czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
				cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
			}
			else if(PL == "p" || PL == "P")
			{
				cout << "\nnazwa pliku 1: mnozenie1.txt\nnazwa pliku 2: mnozenie2.txt\n\n";
				ifstream Wczytaj1("mnozenie1.txt"), Wczytaj2("mnozenie2.txt");
				macierz mMnoz1, mMnoz2;
				Wczytaj1 >> mMnoz1;
				Wczytaj2 >> mMnoz2;
				//cout << "\nMacierz pierwsza wypelniona liczbami z pliku: \n" << mMnoz1 << endl
				//	 << "\nMacierz druga wypelniona liczbami z pliku: \n" << mMnoz2 << endl;
				cout << "\nMacierz wynikowa: \n";
				
				poczatek = clock();		//poczatek mierzenia czasu
				
				cout << mMnoz1 + mMnoz1;
				
				koniec = clock();		// koniec mierzenia czasu
				czas = (double)(koniec - poczatek)/(double)(CLOCKS_PER_SEC);
				cout << endl << "Czas wykonywania [s]: " << setprecision(6) << fixed << czas;
			}
		}
		
		else if(wybor == 7)
		{
			cout << "\nMETODA GAUSSA-SEIDLA" << endl;
			cout << "Wczytac macierz z pliku, czy wypelnic wartosciami losowymi?" << endl;
			cout << "[P - wczytaj macierz z pliku, L - wypelnij losowo]" << endl;
			cout << "Odp. : ";
			cin >> PL;


			if(PL == "l" || PL == "L")
			{
				cout << "\nPodaj rozmiar n macierzy [nxn]: ";
				cin >> roz;
				cout << endl;
				macierz mGaussSeidl(roz,roz+1);
				mGaussSeidl.WypelnijLosowo();
				//cout << "\nMacierz wypelniona losowymi liczbami: \n" << mGauss << endl;
				cout << "\nMacierz wynikowa: \n";
				mGaussSeidl.gaussseidl();
			}
			else if(PL == "p" || PL == "P")
			{
				cout << "\nnazwa pliku: gaussseidl.txt\n\n";
				ifstream Wczytaj("gaussseidl.txt");
				macierz mGaussSeidl;
				Wczytaj >> mGaussSeidl;
				//cout << "\nMacierz wypelniona liczbami z pliku: \n" << mGauss << endl;
				cout << "\nWynik: \n";
				mGaussSeidl.gaussseidl("wektorgasussseidl1.txt","wektorgasussseidl2.txt");
			}
		}

			cout << "\nCzy wykonac inne obliczenia? [T/N]: ";
			cin >> taknie;
			cout << "\n\n";

	}while(taknie == "t" || taknie == "T");

    return 0;
}
